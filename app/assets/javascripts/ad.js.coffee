# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

setCookie = (name, value, days) ->
  if days
    date = new Date()
    date.setTime date.getTime() + (days * 24 * 60 * 60 * 1000)
    expires = "; expires=" + date.toGMTString()
  else
    expires = ""
  document.cookie = name + "=" + value + expires + "; path=/"

getCookie = (name) ->
  nameEQ = name + "="
  ca = document.cookie.split(";")
  i = 0

  while i < ca.length
    c = ca[i]
    c = c.substring(1, c.length)  while c.charAt(0) is " "
    return c.substring(nameEQ.length, c.length)  if c.indexOf(nameEQ) is 0
    i++
  null

deleteCookie = (name) ->
  setCookie name, "", -1

logCookie = (name) ->
		if console
			console.log(name + '=' + getCookie(name));

cookieName = 'giveaway_entry_data'
$(document).ready ->
	logCookie(cookieName)
	$("#entered:checkbox").on "click", ->
		if ($(this).prop("checked"))
			setCookie(cookieName, '' + new Date())
		else
			deleteCookie(cookieName)
		window.location.reload()
		return